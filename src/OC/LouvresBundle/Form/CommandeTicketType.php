<?php

namespace OC\LouvresBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommandeTicketType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('dateVisite',           DateType::class)

            ->remove('nbTicket',           IntegerType::class)

            ->remove('mail',                       EmailType::class)

            ->remove('suivant',                     SubmitType::class)

            ->add('tickets',                        CollectionType::class, array(
                'entry_type'    =>      TicketType::class,
                'allow_add'     =>      true,
                'label' => 'Veuillez remplir les champs ci-dessous :',
                'attr' => array('class'=>'ticket_form')))

            ->add('suivant',                SubmitType::class)

        ;
    }

    public function getParent(){
        return CommandeType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OC\LouvresBundle\Entity\Commande',
        ));
    }

}
