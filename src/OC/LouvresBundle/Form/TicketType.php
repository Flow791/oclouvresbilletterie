<?php

namespace OC\LouvresBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TicketType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('typeTicket',     ChoiceType::class, array(
                'choices'  => array(
                    'Journée' => true,
                    '1/2 Journée' => false,),
                'expanded' => true,
                'multiple' => false,
                'label'=>'Type de ticket :',))

            ->add('nom',            TextType::class, array(
                'label'=>'Nom :',
                'attr' => array('class'=>'entree')))

            ->add('prenom',         TextType::class, array(
                'label'=>'Prenom :',
                'attr' => array('class'=>'entree')))

            ->add('dateNaissance',  DateType::class, array(
                'years' => (range(2016,1900)),
                'label'=>'Date de naissance :',
                'attr' => array('class'=>'entree')))

            ->add('pays',           TextType::class, array(
                'label'=>'Pays :',
                'attr' => array('class'=>'entree')))

            ->add('tarifReduit',    CheckboxType::class, array(
                'required' => false ,
                'label'=>'Tarif reduit',))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OC\LouvresBundle\Entity\Ticket',
            'validation_groups' => array('Ticket')
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'oc_louvresbundle_ticket';
    }


}
