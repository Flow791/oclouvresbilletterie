<?php

namespace OC\LouvresBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommandeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateVisite',         DateType::class, array(
                'widget'    =>  'single_text',
                'format'    =>  'yyyy-MM-dd',
                'attr'      =>  array(
                    'class' =>  'date entree'),
                'label'     =>  'Date de la visite :',))

            ->add('nbTicket',           IntegerType::class, array(
                'label'=>'Nombre de tickets :',
                'attr' => array('class'=>'entree')))

            ->add('mail',               EmailType::class,array (
                'label'=>'Email :',
                'attr' => array('class'=>'entree')))

            ->add('suivant',            SubmitType::class,array(
                'label'=>'Suivant',))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OC\LouvresBundle\Entity\Commande',
            'validation_groups' => array('stepOne')
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'oc_louvresbundle_commande';
    }

}
