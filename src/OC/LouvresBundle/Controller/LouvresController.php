<?php

namespace OC\LouvresBundle\Controller;

use OC\LouvresBundle\Entity\Commande;
use OC\LouvresBundle\Form\CommandeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use OC\LouvresBundle\Form\CommandeTicketType;

class LouvresController extends Controller
{

    public function indexAction(){
        return $this->render('OCLouvresBundle:Louvres:index.html.twig');
    }

    /**
     * Action used to create a command
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function commandeAction(Request $request)
    {

        $session = $request->getSession();

        $commande = new Commande();
        $form = $this->createForm(CommandeType::class,$commande);
        $form->handleRequest($request);

        //Action du formulaire
        if ($form->isValid()) {

            //Recuperation du nombre de ticket le jour de la visite.
            $dateVisite = $commande->getDateVisite()->format('Y-m-d');
            $nbTicket = $this->getDoctrine()
                ->getManager()
                ->getRepository('OCLouvresBundle:Commande')
                ->getNbTicket($dateVisite);

            if ($nbTicket + $commande->getNbTicket() < 1000){

                $commande->setSessionId($session->getId());
                $em = $this->getDoctrine()->getManager();
                $em->persist($commande);
                $em->flush();

                return $this->redirectToRoute('oc_louvres_ticket', array('id' => $commande->getId()));
            }
        }

        //Retourne la vue a générée
        return $this->render('OCLouvresBundle:Louvres:commande.html.twig', array(
            'commandeForm' => $form->createView(),
        ));
    }


    public function ticketAction(Request $request, $id)
    {

        //Test sur la session et la commande en cours.
        $commande = $this->get('oc_louvres.checkSession')->checkSession($request,$id);

        if (!$commande){
            $this->addFlash("error", "Cette commande n'existe pas!");
            return $this->redirectToRoute('oc_louvres_homepage');
        }


        //Test du type de billet reservable en fonction du jour et de l'heure.
        $dateVisite = $commande->getDateVisite();
        $isReservable = $this->get('oc_louvres.testdispo')->isReservable($dateVisite);


        //Creation du formulaire CommandeTicket
        $form = $this->createForm(CommandeTicketType::class, $commande);
        $form->handleRequest($request);

        //Action de l'envoi du formulaire si POST
        if ($form->isValid()) {

            //calcul du prix de chaque ticket;
            $this->get('oc_louvres.calculPrix')->calculPrix($commande);

            //Ajoute chaque ticket du POST dans l'Array tickets de l'objet Commande
            foreach ($commande->getTickets() as $ticket){
                $commande->addTicket($ticket);
            }

            //Ajout des ticket contenu dans l'ArrayList dans la BDD
            $em = $this->getDoctrine()->getManager();
            $em->persist($commande);
            $em->flush();

            return $this->redirectToRoute('oc_louvres_checkout', array('id' => $commande->getId()));
        }

        // Sinon (GET) creation de la vue.
        return $this->render('OCLouvresBundle:Louvres:ticket.html.twig', array(
            'ticketForm' => $form->createView(),
            'nbTicket'   => $commande->getNbTicket(),
            'isReservable'    =>  $isReservable,
        ));
    }


    public function checkOutAction(Request $request, $id){

        //Verification de la commande et de la session en cours
        $commande = $this->get('oc_louvres.checkSession')->checkSession($request,$id);
        if (!$commande){
            return $this->redirectToRoute('oc_louvres_homepage');
        }


        //Calcul du prix total.
        $prixTotal = $this->get('oc_louvres.calculPrix')->getPrix($commande);


        if ($request->isMethod('POST')) {

            //Verification du prix. Superieur à zéro -> paiement et envoi des billet.
            if ($prixTotal > 0){
                //Recuperation de la clé Stripe
                \Stripe\Stripe::setApiKey($this->getParameter('stripe_key'));

                // Recuperation des données de carte
                $token = $request->request->get('stripeToken');

                \Stripe\Customer::create(array(
                    "description" => "Achteur de billet Musée du Louvres",
                    "email" => $commande->getMail()
                ));

                // Création de la charge.
                $charge = \Stripe\Charge::create(array(
                    "amount" => ($prixTotal * 100), //Prix en centimes
                    "currency" => "eur",
                    "source" => $token,
                    "description" => "Paiement ticket n°" . $id
                ));

                //Envoi du mail contenant les tickets retour à la home avec message succes
                $this->get('oc_louvres.mailSender')->mailSender($commande, $prixTotal);
                $this->addFlash(
                    'success',
                    'Paiement effectué avec succès, vous recevrez vos tickets par mail!'
                );
                return $this->redirectToRoute('oc_louvres_homepage');
            }

            //Sinon retour à l'accueil car le public serait trop jeune.
            elseif ($prixTotal === 0){
                $this->addFlash(
                    'error',
                    'Vous ne pouvez pas commander avec seulement un/des billet(s) -4ans! Veuillez recommencer..'
                );
                return $this->redirectToRoute('oc_louvres_homepage');
            }
        }

        return $this->render('OCLouvresBundle:Louvres:checkout.html.twig', array(
            'id' => $id,
            'tickets' => $commande->getTickets(),
            'prixTotal' => $prixTotal,
            'nbTicket' => $commande->getNbTicket(),
            'email' => $commande->getMail(),
            'dateVisite' => $commande->getDateVisite()->format('d-m-Y')
        ));
    }
}
