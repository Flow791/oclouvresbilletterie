<?php

namespace OC\LouvresBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Commande
 *
 * @ORM\Table(name="commande")
 * @ORM\Entity(repositoryClass="OC\LouvresBundle\Repository\CommandeRepository")
 */
class Commande
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCommande", type="datetime")
     */
    private $dateCommande;

    /**
     * @var \Date
     *
     * @ORM\Column(name="dateVisite", type="date")
     * @Assert\NotBlank(
     *     message="Selectionnez une date",
     *     groups={"stepOne"})
     */
    private $dateVisite;

    /**
     * @var int
     *
     * @ORM\Column(name="nbTicket", type="integer")
     * @Assert\Range(
     *     min="1",
     *     max="50",
     *     minMessage="Commandez au moins 1 ticket",
     *     maxMessage="Commande de plus de 50 tickets impossible",groups={"stepOne"})
     */
    private $nbTicket;

    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=255)
     * @Assert\NotBlank(
     *     message="Entrez une adresse mail",
     *     groups={"stepOne"})
     */
    private $mail;

    /**
     * @var string
     *
     * @ORM\Column(name="sessionID", type="string", length=255)
     */
    private $sessionID;

    /**
     * @ORM\OneToMany(targetEntity="\OC\LouvresBundle\Entity\Ticket",
     *     mappedBy="commande",
     *     cascade={"persist"})
     */
    private $tickets;

    /**
     * @var string
     *
     * @ORM\Column(name="commandeNum", type="string", length=255)
     */
    private $commandeNum;


    public function __construct()
    {
        $this->setDateCommande(new \DateTime());
        $this->tickets = new ArrayCollection();
        $this->setCommandeNum();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCommande
     *
     * @param \DateTime $dateCommande
     *
     * @return Commande
     */
    public function setDateCommande($dateCommande)
    {
        $this->dateCommande = $dateCommande;

        return $this;
    }

    /**
     * Get dateCommande
     *
     * @return \DateTime
     */
    public function getDateCommande()
    {
        return $this->dateCommande;
    }

    /**
     * Set nbTicket
     *
     * @param integer $nbTicket
     *
     * @return Commande
     */
    public function setNbTicket($nbTicket)
    {
        $this->nbTicket = $nbTicket;

        return $this;
    }

    /**
     * Get nbTicket
     *
     * @return int
     */
    public function getNbTicket()
    {
        return $this->nbTicket;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return Commande
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set dateVisite
     *
     * @param \DateTime $dateVisite
     *
     * @return Commande
     */
    public function setDateVisite($dateVisite)
    {
        $this->dateVisite = $dateVisite;

        return $this;
    }

    /**
     * Get dateVisite
     *
     * @return \DateTime
     */
    public function getDateVisite()
    {
        return $this->dateVisite;
    }

    /**
     * Set sessionID
     *
     * @param string $sessionID
     *
     * @return Commande
     */
    public function setSessionID($sessionID)
    {
        $this->sessionID = $sessionID;

        return $this;
    }

    /**
     * Get sessionID
     *
     * @return string
     */
    public function getSessionID()
    {
        return $this->sessionID;
    }

    /**
     * Add ticket
     *
     * @param \OC\LouvresBundle\Entity\Ticket $ticket
     *
     * @return Commande
     */
    public function addTicket(\OC\LouvresBundle\Entity\Ticket $ticket)
    {
        $this->tickets[] = $ticket;

        $ticket->setCommande($this);

        return $this;
    }

    /**
     * Remove ticket
     *
     * @param \OC\LouvresBundle\Entity\Ticket $ticket
     */
    public function removeTicket(\OC\LouvresBundle\Entity\Ticket $ticket)
    {
        $this->tickets->removeElement($ticket);
    }

    /**
     * Get tickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTickets()
    {
        return $this->tickets;
    }



    /**
     * Set commandeNum
     *
     * @param string $commandeNum
     *
     * @return Commande
     */
    public function setCommandeNum()
    {
        $this->commandeNum = time() . random_int(000000,999999);

        return $this;
    }

    /**
     * Get commandeNum
     *
     * @return string
     */
    public function getCommandeNum()
    {
        return $this->commandeNum;
    }
}
