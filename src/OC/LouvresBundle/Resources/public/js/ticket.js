


    $(document).ready(function() {
        // On récupère la balise <div> en question qui contient l'attribut « data-prototype » qui nous intéresse.


        $container = $('div#commande_ticket_tickets');

        // On définit un compteur unique pour nommer les champs qu'on va ajouter dynamiquement
        var index = $container.find(':input').length;

        // On ajoute un nouveau champ à chaque tour de boucle.
        for (var i = 0; i < nbTicket; i++){
            addTicket($container);
        }

        // On ajoute un premier champ automatiquement
        if (index == 0) {
            addTicket($container);
        }

        // La fonction qui ajoute un formulaire TicketType
        function addTicket($container) {
            // Dans le contenu de l'attribut « data-prototype », on remplace :
            // - le texte "__name__label__" qu'il contient par le label du champ
            // - le texte "__name__" qu'il contient par le numéro du champ
            var template = $container.attr('data-prototype')
                    .replace(/__name__label__/g, 'Ticket n°' + (index+1))
                    .replace(/__name__/g,        index)
                ;

            // On crée un objet jquery qui contient ce template
            var $prototype = $(template);

            //Creation d'une bande pour distinguer les differents tickets.
            var $bande = '<p class="ticket_fin"></p>';

            // On ajoute le prototype modifié à la fin de la balise <div>
            $container.append($bande);
            $container.append($prototype);




            // Enfin, on incrémente le compteur pour que le prochain ajout se fasse avec un autre numéro
            index++;
        }

        if (!isReservable) {
            $('.radio [value="1"]').remove();
        }

    });
