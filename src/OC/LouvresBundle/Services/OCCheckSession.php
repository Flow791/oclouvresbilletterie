<?php

namespace OC\LouvresBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;


class OCCheckSession
{
    private $em;
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param $id
     * @return bool
     */
    public function checkSession(Request $request, $id){
        //Verification de l'ID de commande et de l'ID de session
        $sessionId = $request->getSession()->getId();
        $commande = $this->em
            ->getRepository('OCLouvresBundle:Commande')
            ->findSessionID($id);

        $commandeSessionId = $commande->getSessionID();

        //Verification que la session est toujours la même.
        if ($sessionId != $commandeSessionId){
            return false;
        }

        return $commande;
    }
}