<?php

namespace OC\LouvresBundle\Services;


use OC\LouvresBundle\Entity\Commande;

class OCMailSender
{

    protected $mailer;
    protected $templating;

    public function __construct($mailer, $container)
    {
        $this->mailer = $mailer;
        $this->templating = $container->get('templating');
    }

    public function mailConstruct($sujet, $envoyeur, $destinataire, $body){

        $message = \Swift_Message::newInstance()
            ->setSubject($sujet)
            ->setFrom($envoyeur)
            ->setTo($destinataire)
            ->setBody( $body, 'text/html')
        ;
        return $this->mailer->send($message);
    }

    public function mailSender(Commande $commande, $prixTotal){

        $sujet = 'Vos tickets pour le Musée du Louvres';

        $envoyeur = 'testdevfb01@gmail.com';

        $destinataire = $commande->getMail();

        $body = $this->templating->render('OCLouvresBundle:Louvres:tickets.html.twig',
            array('tickets' => $commande->getTickets(), 'numeroCommande' => $commande->getCommandeNum(),
                'dateCommande' => $commande->getDateCommande()->format('d-m-Y'),'prixTotal' => $prixTotal,
                'nbTicket' => $commande->getNbTicket(), 'email' => $commande->getMail(),
                'dateVisite' => $commande->getDateVisite()->format('d-m-Y'))
        );

        return $this->mailConstruct($sujet, $envoyeur, $destinataire, $body);
    }

}