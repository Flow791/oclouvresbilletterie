<?php

namespace OC\LouvresBundle\Services;



use OC\LouvresBundle\Entity\Commande;

class OCCalculPrix
{

    //Defini le prix d'un ticket en appelant la methode definiTarif
    public static function calculPrix(Commande $commande){

        $tickets = $commande->getTickets();

        foreach ($tickets as $ticket){
            $dateNaissance = $ticket->getDateNaissance();
            $tarifReduit = $ticket->getTarifReduit();

            //Recuperation et definition du prix de chaque billet.
            $prix = OCCalculPrix::definiTarif($dateNaissance, $tarifReduit);
            $ticket->setPrix($prix);
        }

        return 1;
    }

    //Defini le tarif du ticket.
    public static function definiTarif($dateNaissance, $tarifReduit){

        $age = intval(OCCalculPrix::calculeAge($dateNaissance));

        if ($tarifReduit === false || $age < "12"){

            switch ($age){
                case ($age === "0"):
                    $tarif = 0;
                    break;
                case  ($age >= "60"):
                     $tarif = 12;
                    break;
                case ($age >= "12" && $age < "60"):
                     $tarif = 16;
                    break;
                case ($age >= "4" && $age < "12"):
                     $tarif = 8;
                    break;
                case ($age < "4"):
                     $tarif = 0;
                    break;
                default:
                     $tarif = 0;
                    break;
            }
        }
        else {
            $tarif = 10;
        }

        return $tarif;
    }


    //Calcul de l'age
    public static function calculeAge(\DateTime $dateNaissance){

        $date_today = new \DateTime("now");
        $dateNaissance = new \DateTime($dateNaissance->format('Y-m-d'));

        $interval = $dateNaissance->diff($date_today);

        return $interval->format('%y');
    }


    //Retourne le prix total de la commande
    public static function getPrix(Commande $commande){

        $prixTotal = 0;
        $tickets = $commande->getTickets();

        foreach ($tickets as $ticket){
            $prix = $ticket->getPrix();
            $prixTotal += $prix;
        }

        return $prixTotal;
    }

}