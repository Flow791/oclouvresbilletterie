<?php

namespace OC\LouvresBundle\Services;


class OCTestDispo
{
    //Defini si le ticket est reservable ou non pour la journée
    public static function isReservable($dateVisite){

        date_default_timezone_set('Europe/Paris');

        if ( $dateVisite <= new \DateTime() && date('H') >= 14 ){
            return 0;
        }

        return 1;

    }

}