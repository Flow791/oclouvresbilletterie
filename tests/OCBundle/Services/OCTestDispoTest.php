<?php

use OC\LouvresBundle\Services\OCCalculPrix;

class OCTestDispoTest extends PHPUnit_Framework_TestCase
{

    public function testIsReservable(){

        $this->assertEquals(0, OC\LouvresBundle\Services\OCTestDispo::isReservable(new DateTime('2016-08-01 15:00:00')));
        $this->assertEquals(1, OC\LouvresBundle\Services\OCTestDispo::isReservable(new DateTime('2018-08-01 15:00:00')));
    }


}
