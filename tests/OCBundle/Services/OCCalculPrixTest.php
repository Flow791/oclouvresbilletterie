<?php

use OC\LouvresBundle\Services\OCCalculPrix;

class OCCalculPrixTest extends PHPUnit_Framework_TestCase
{

    public function testCalculAge(){

        $this->assertEquals(20, OC\LouvresBundle\Services\OCCalculPrix::calculeAge(new DateTime('1996-08-01')));
        $this->assertEquals(0, OC\LouvresBundle\Services\OCCalculPrix::calculeAge(new DateTime('2016-08-01')));

    }

    public function testDefiniTarif(){
        $this->assertEquals(12, OC\LouvresBundle\Services\OCCalculPrix::definiTarif(new DateTime('1946-08-01'),false));
        $this->assertEquals(0, OC\LouvresBundle\Services\OCCalculPrix::definiTarif(new DateTime('2015-12-05'),true));

    }


}
